require('dotenv').config()

module.exports = {
  client: 'pg',
  connection: {
    host: process.env.DB_HOST,
    database: process.env.DB_NAME
  },
  migrations: {
    directory: 'dist/db/migrations',
    tableName: 'knex_migrations'
  },
  seeds: {
    directory: 'dist/db/seeds'
  }
}
