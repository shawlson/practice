import fastify = require('fastify')

export type League = {
  name: string
  description: string
}

export type Team = {
  id: number
  represents: string
  name: string
  primaryLeague: string
}

export type TeamLeagueHistory = {
  league: string
  seasons: number[]
}

export type Game = {
  id: number
  league: string
  season: number
  date: string
  home: Team
  home_score: number
  away: Team
  away_score: number
  title?: string
  venue?: string
}

export type Resource = {
  id: number
  game: number
  gameSegment: string
  type: string
  url: string
}

export type Account = {
  username: string
  role: string
}

export const enum AccountRole {
  ADMIN = 'admin',
  CONTRIBUTOR = 'contributor',
  FRIEND = 'friend'
}

export type Session = {
  id: string
  signed: boolean
  expired: boolean
  account: Account
}

declare module 'fastify' {
  interface FastifyRequest<
    HttpRequest,
    Query = fastify.DefaultQuery,
    Params = fastify.DefaultParams,
    Headers = fastify.DefaultHeaders,
    Body = any
  > {
    session: Session
  }
}
