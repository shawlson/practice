import fastify = require('fastify')
import { Server, IncomingMessage, ServerResponse } from 'http'

import { AccountRole } from './types'
import { authorizeTo } from './middleware'
import { LeagueSchemas, TeamSchemas, GameSchemas, ResourceSchemas, AccountSchemas } from './schemas'
import {
  LeagueController,
  TeamController,
  GameController,
  ResourceController,
  AccountController
} from './controllers'

export async function LeagueRoutes(
  fastify: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse>,
  options
) {
  fastify.route({
    method: 'GET',
    url: '/leagues',
    schema: LeagueSchemas.query,
    handler: LeagueController.query
  })

  fastify.route({
    method: 'GET',
    url: '/leagues/:name',
    schema: LeagueSchemas.get,
    handler: LeagueController.get
  })
}

export async function TeamRoutes(
  fastify: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse>,
  options
) {
  fastify.route({
    method: 'GET',
    url: '/teams',
    schema: TeamSchemas.query,
    handler: TeamController.query
  })

  fastify.route({
    method: 'GET',
    url: '/teams/:id',
    schema: TeamSchemas.get,
    handler: TeamController.get
  })
}

export async function GameRoutes(
  fastify: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse>,
  options
) {
  fastify.route({
    method: 'GET',
    url: '/games',
    schema: GameSchemas.query,
    handler: GameController.query
  })

  fastify.route({
    method: 'POST',
    url: '/games',
    preValidation: authorizeTo([AccountRole.ADMIN, AccountRole.CONTRIBUTOR]),
    schema: GameSchemas.post,
    handler: GameController.post
  })

  fastify.route({
    method: 'GET',
    url: '/games/:id',
    schema: GameSchemas.get,
    handler: GameController.get
  })

  fastify.route({
    method: 'PATCH',
    url: '/games/:id',
    preValidation: authorizeTo([AccountRole.ADMIN, AccountRole.CONTRIBUTOR]),
    schema: GameSchemas.patch,
    handler: GameController.patch
  })

  fastify.route({
    method: 'DELETE',
    url: '/games/:id',
    preValidation: authorizeTo([AccountRole.ADMIN]),
    schema: GameSchemas.delete,
    handler: GameController.delete
  })
}

export async function ResourceRoutes(
  fastify: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse>,
  options
) {
  fastify.route({
    method: 'GET',
    url: '/resources',
    schema: ResourceSchemas.query,
    handler: ResourceController.query
  })

  fastify.route({
    method: 'POST',
    url: '/resources',
    preValidation: authorizeTo([AccountRole.ADMIN, AccountRole.CONTRIBUTOR]),
    schema: ResourceSchemas.post,
    handler: ResourceController.post
  })

  fastify.route({
    method: 'GET',
    url: '/resources/:id',
    schema: ResourceSchemas.get,
    handler: ResourceController.get
  })

  fastify.route({
    method: 'PATCH',
    url: '/resources/:id',
    preValidation: authorizeTo([AccountRole.ADMIN, AccountRole.CONTRIBUTOR]),
    schema: ResourceSchemas.patch,
    handler: ResourceController.patch
  })

  fastify.route({
    method: 'DELETE',
    url: '/resources/:id',
    preValidation: authorizeTo([AccountRole.ADMIN]),
    schema: ResourceSchemas.delete,
    handler: ResourceController.delete
  })
}

export async function AccountRoutes(
  fastify: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse>,
  options
) {
  fastify.route({
    method: 'POST',
    url: '/users',
    schema: AccountSchemas.post,
    handler: AccountController.post
  })

  fastify.route({
    method: 'POST',
    url: '/login',
    schema: AccountSchemas.login,
    handler: AccountController.login
  })

  fastify.route({
    method: 'POST',
    url: '/logout',
    handler: AccountController.logout
  })

  fastify.route({
    method: 'PATCH',
    url: '/users/:username',
    schema: AccountSchemas.patch,
    handler: AccountController.patch
  })
}
