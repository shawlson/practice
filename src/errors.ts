class BaseError extends Error {
  constructor(message: string) {
    super(message)
    this.name = this.constructor.name
  }
}

export class InvalidDataError extends BaseError {
  fields: string | string[]

  constructor(message: string, fields?: string | string[]) {
    super(message)
    this.fields = fields || null
  }
}

export class ConflictError extends BaseError {
  constructor(message: string) {
    super(message)
  }
}
