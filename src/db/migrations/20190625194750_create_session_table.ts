import * as Knex from 'knex'

export async function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable('session', table => {
      table
        .uuid('id')
        .defaultTo(knex.raw('gen_random_uuid()'))
        .primary()
      table.string('username', 16).notNullable()
      table
        .timestamp('expires')
        .defaultTo(
          knex.raw(`current_timestamp + current_setting('recap.session.length')::interval`)
        )
        .notNullable()
      table.text('signed').notNullable()
      table
        .foreign('username', 'Account exists')
        .references('username')
        .inTable('account')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })

    .raw(
      `
      CREATE FUNCTION sign_session()
      RETURNS TRIGGER AS
      $$
        BEGIN
          SELECT INTO NEW.signed
            encode(hmac(NEW.id::text, current_setting('recap.session.key'), 'sha256'), 'hex');

          RETURN NEW;
        END;
      $$
      LANGUAGE plpgsql
      `
    )

    .raw(
      `
      CREATE TRIGGER sign_session
      BEFORE INSERT ON session
      FOR EACH ROW EXECUTE FUNCTION sign_session()
      `
    )
}

export async function down(knex: Knex): Promise<any> {
  return knex.schema.dropTable('session').raw('DROP FUNCTION sign_session()')
}
