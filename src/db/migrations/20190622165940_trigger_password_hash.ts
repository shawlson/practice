import * as Knex from 'knex'

export async function up(knex: Knex): Promise<any> {
  return knex.schema
    .raw(
      `
      CREATE FUNCTION hash_password()
      RETURNS trigger
      AS $$
        BEGIN
          SELECT
            crypt(NEW.password, gen_salt('bf'))
          INTO
            NEW.password;

          RETURN NEW;
        END;
      $$ LANGUAGE plpgsql  
      `
    )
    .raw(
      `
      CREATE TRIGGER hash_password
      BEFORE INSERT OR UPDATE OF password
      ON account
      FOR EACH ROW
      EXECUTE FUNCTION hash_password()
      `
    )
}

export async function down(knex: Knex): Promise<any> {
  return knex.schema.raw('DROP TRIGGER hash_password ON account').raw('DROP FUNCTION hash_password')
}
