import * as Knex from 'knex'

export async function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable('league', table => {
      table.string('name', 8).primary()
      table.string('description', 32).notNullable()
    })

    .createTable('season', table => {
      table.string('league', 8).notNullable()
      table.integer('start_year').notNullable()
      table.specificType('span', 'daterange').notNullable()
      table.primary(['league', 'start_year'])
      table
        .foreign('league', 'League exists')
        .references('name')
        .inTable('league')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
    })

    .raw(
      `
      ALTER TABLE season
      ADD CONSTRAINT "start_year matches span"
      CHECK (date_part('year', lower(span)) = start_year),
      ADD CONSTRAINT "Year makes sense"
      CHECK (start_year BETWEEN 2000 and 2030),
      ADD CONSTRAINT "League seasons don't overlap"
      EXCLUDE USING gist (league WITH =, span WITH &&)
      `
    )

    .createTable('franchise', table => {
      table
        .integer('id')
        .unsigned()
        .primary()
      table.integer('iteration').notNullable()
      table.string('primary_league').notNullable()
      table
        .boolean('active')
        .defaultTo(true)
        .notNullable()
      table
        .foreign('primary_league', 'League exists')
        .references('name')
        .inTable('league')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
    })

    .createTable('franchise_iteration', table => {
      table.integer('franchise').notNullable()
      table
        .integer('iteration')
        .defaultTo(1)
        .notNullable()
      table.string('represents', 64).notNullable()
      table.string('name', 64).notNullable()
      table.primary(['franchise', 'iteration'])
      table
        .foreign('franchise', 'Franchise exists')
        .references('id')
        .inTable('franchise')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })

    .raw(
      `
      ALTER TABLE franchise_iteration
      ADD CONSTRAINT "Positive iteration"
      CHECK (iteration > 0)
      `
    )

    .raw(
      `
      ALTER TABLE franchise
      ADD CONSTRAINT "Franchise iteration exists"
      FOREIGN KEY (id, iteration)
      REFERENCES franchise_iteration(franchise, iteration),
      ALTER CONSTRAINT "Franchise iteration exists"
      DEFERRABLE INITIALLY DEFERRED
      `
    )

    .raw(
      `
      ALTER TABLE franchise_iteration
      ALTER CONSTRAINT "Franchise exists"
      DEFERRABLE INITIALLY DEFERRED
      `
    )

    .createTable('season_franchise_iteration', table => {
      table.string('league', 8).notNullable()
      table.integer('season').notNullable()
      table.integer('franchise').notNullable()
      table.integer('iteration').notNullable()
      table.primary(['league', 'season', 'franchise'])
      table
        .foreign('league', 'League exists')
        .references('name')
        .inTable('league')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
      table
        .foreign(['league', 'season'], 'Season exists')
        .references(['league', 'start_year'])
        .inTable('season')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
      table
        .foreign('franchise', 'Franchise exists')
        .references('id')
        .inTable('franchise')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
      table
        .foreign(['franchise', 'iteration'], 'Franchise iteration exists')
        .references(['franchise', 'iteration'])
        .inTable('franchise_iteration')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
    })

    .createTable('game', table => {
      table.increments('id')
      table.string('league', 8).notNullable()
      table.integer('season').notNullable()
      table.date('game_date').notNullable()
      table.integer('home_franchise').notNullable()
      table.integer('home_score').notNullable()
      table.integer('away_franchise').notNullable()
      table.integer('away_score').notNullable()
      table.string('title', 64)
      table.string('venue', 32)
      table
        .foreign('league', 'League exists')
        .references('name')
        .inTable('league')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
      table
        .foreign(['league', 'season'], 'Season exists')
        .references(['league', 'start_year'])
        .inTable('season')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
      table
        .foreign('home_franchise', 'Home franchise exists')
        .references('id')
        .inTable('franchise')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
      table
        .foreign('away_franchise', 'Away franchise exists')
        .references('id')
        .inTable('franchise')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
      table
        .foreign(
          ['league', 'season', 'home_franchise'],
          'Home franchise participates in league season'
        )
        .references(['league', 'season', 'franchise'])
        .inTable('season_franchise_iteration')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
      table
        .foreign(
          ['league', 'season', 'away_franchise'],
          'Away franchise participates in league season'
        )
        .references(['league', 'season', 'franchise'])
        .inTable('season_franchise_iteration')
        .onUpdate('CASCADE')
        .onDelete('RESTRICT')
    })

    .raw(
      `
      ALTER TABLE game
      ADD CONSTRAINT "Franchise does not play itself"
      CHECK (home_franchise != away_franchise),
      ADD CONSTRAINT "Postive scores"
      CHECK (home_score >= 0 AND away_score >= 0)
      `
    )

    .createTable('resource', table => {
      table.increments('id')
      table.integer('game').notNullable()
      table.string('title', 128)
      table
        .enu('game_segment', ['meta', 'pre-game', 'in-game', 'post-game'], {
          useNative: true,
          enumName: 'resource_game_segment',
          existingType: false
        })
        .notNullable()
      table
        .enu('type', ['link'], { useNative: true, enumName: 'resource_type', existingType: false })
        .notNullable()
      // "link" resource
      table.string('url', 128)
      table
        .foreign('game')
        .references('id')
        .inTable('game')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })

    .createTable('link_resource_whitelist', table => {
      table.string('domain').primary()
    })

    .createTable('account', table => {
      table.string('username', 16).primary()
      table.text('password').notNullable()
      table
        .enu('role', ['admin', 'contributor', 'friend'], {
          useNative: true,
          enumName: 'account_type',
          existingType: false
        })
        .defaultTo('friend')
    })
}

export async function down(knex: Knex): Promise<any> {
  return knex.schema
    .dropTable('account')
    .raw('DROP TYPE account_type')
    .dropTable('link_resource_whitelist')
    .dropTable('resource')
    .raw('DROP TYPE resource_type')
    .raw('DROP TYPE resource_game_segment')
    .dropTable('game')
    .dropTable('season_franchise_iteration')
    .raw('DROP TABLE franchise, franchise_iteration')
    .dropTable('season')
    .dropTable('league')
}
