import * as Knex from 'knex'
import * as league from './leagues/nfl/league.json'
import * as seasons from './leagues/nfl/seasons.json'
import * as franchises from './leagues/nfl/franchises.json'
import * as franchiseIterations from './leagues/nfl/franchise_iterations.json'
import * as seasonFranchiseIterations from './leagues/nfl/season_franchise_iterations.json'

export async function seed(knex: Knex): Promise<any> {
  const transaction = knex.transaction(async trx => {
    const leagueQuery = knex('league').insert(league)
    const seasonsQuery = knex('season').insert(seasons)
    const franchisesQuery = knex('franchise').insert(franchises)
    const franchiseIterationsQuery = knex('franchise_iteration').insert(franchiseIterations)
    const seasonFranchiseIterationsQuery = knex('season_franchise_iteration').insert(
      seasonFranchiseIterations
    )

    await trx.raw(
      `
      ? ON CONFLICT (name) DO UPDATE 
      SET description = EXCLUDED.description
      `,
      leagueQuery
    )

    await trx.raw(
      `
      ? ON CONFLICT (league, start_year) DO UPDATE
      SET span = EXCLUDED.span
      `,
      seasonsQuery
    )

    await trx.raw(
      `
      ? ON CONFLICT (id) DO UPDATE SET
      iteration = EXCLUDED.iteration,
      primary_league = EXCLUDED.primary_league,
      active = EXCLUDED.active
      `,
      franchisesQuery
    )

    await trx.raw(
      `
      ? ON CONFLICT (franchise, iteration) DO UPDATE SET
      represents = EXCLUDED.represents,
      name = EXCLUDED.name
      `,
      franchiseIterationsQuery
    )

    await trx.raw(
      `
      ? ON CONFLICT (league, season, franchise) DO UPDATE
      SET iteration = EXCLUDED.iteration
      `,
      seasonFranchiseIterationsQuery
    )
  })

  return await transaction
}
