import * as Knex from 'knex'
import { AccountRole } from '../../types'

export async function seed(knex: Knex): Promise<any> {
  const query = knex('account').insert({
    username: 'admin',
    password: 'changeme',
    role: AccountRole.ADMIN
  })

  return knex.raw('? ON CONFLICT DO NOTHING', query)
}
