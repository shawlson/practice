import * as Knex from 'knex'

export async function seed(knex: Knex): Promise<any> {
  const domains = [
    { domain: 'reddit.com' },
    { domain: 'streamable.com' },
    { domain: 'youtube.com' },
    { domain: 'apnews.com' },
    { domain: 'twitter.com' }
  ]

  const query = knex('link_resource_whitelist').insert(domains)
  return knex.raw('? ON CONFLICT DO NOTHING', query)
}
