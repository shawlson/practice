import knex = require('knex')
import { types } from 'pg'

import * as RawErrorCodes from './errors/RawErrorCodes.json'
import * as RawErrorClasses from './errors/RawErrorClasses.json'

// Tell node-postgres to return dates
// as ISO 8601 strings
const TIMESTAMP_OID = 1082
types.setTypeParser(TIMESTAMP_OID, (date: string): string => date)

const db = knex({
  client: 'postgresql',
  connection: {
    host: process.env.DB_SOCKET,
    database: process.env.DB_NAME
  },
  pool: {
    afterCreate: (connection, done) => {
      connection.query(`SET recap.session.key = '${process.env.SESSION_KEY}'`, err => {
        if (!err) {
          connection.query(`SET recap.session.length = '${process.env.SESSION_LENGTH}'`, err => {
            done(err, connection)
          })
        } else {
          done(err, connection)
        }
      })
    }
  }
})

// Make errors emitted by node-postgres more useful
db.on('query-error', err => {
  err.name = 'PostgresError'
  err.errorCondition = RawErrorCodes[err.code] || 'unknown'
  err.errorClass = RawErrorClasses[err.code.substring(0, 2)] || 'unknown'
  Object.keys(err).forEach(key => err[key] === undefined && delete err[key])
})

export default db
