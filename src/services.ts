//@ts-nocheck
import parseDomain = require('parse-domain')

import db from './db'
import { SortOrder, PgErrorClass, PgError } from './db/types'

import { InvalidDataError, ConflictError } from './errors'
import {
  League,
  Team,
  TeamLeagueHistory,
  Game,
  Resource,
  Session,
  Account,
  AccountRole
} from './types'

export const LeagueService = {
  /**
   * Get a league's description and seasons by its name
   *
   * @param name The name of a league
   * @returns The league's description and seasons, or undefined if no
   *          such league is found
   */
  get: async (name: string): Promise<League & { seasons: number[] }> => {
    return db('league')
      .join('season', 'season.league', 'league.name')
      .first('name', 'description', db.raw('array_agg(season.start_year) as seasons'))
      .groupBy('league.name')
      .where({ name })
  },

  /**
   * Query for leagues that meet criteria
   *
   * @param names League names
   * @returns All leagues that meet criteria
   */
  query: async (names?: string[]): Promise<League[]> => {
    let query = db('league').select('name', 'description')
    if (names) {
      query = query.whereIn('name', names)
    }

    return query
  }
}

export const TeamService = {
  /**
   * Get a team by its id
   *
   * @param id The team's id
   * @returns The team, or undefined if no such team is found
   */
  get: async (id: number): Promise<Team> => {
    const query = db
      .select()
      .column('franchise.id')
      .column('franchise_iteration.represents')
      .column('franchise_iteration.name')
      .column('franchise.primary_league as primaryLeague')
      .from('franchise')
      .join('franchise_iteration', function() {
        this.on('franchise_iteration.franchise', 'franchise.id').andOn(
          'franchise_iteration.iteration',
          'franchise.iteration'
        )
      })
      .where('franchise.id', id)
      .first()

    return query
  },

  /**
   * Get the leagues and seasons in which a team has participated
   *
   * @param id The team's id
   * @returns Each league a team has competed in, and
   *          the seasons in which they competed
   */
  getLeagueHistory: async (id: number): Promise<TeamLeagueHistory[]> => {
    const query = db
      .select(
        'season_franchise_iteration.league',
        db.raw('array_agg(season_franchise_iteration.season) as seasons')
      )
      .from('franchise')
      .join('season_franchise_iteration', 'season_franchise_iteration.franchise', 'franchise.id')
      .where('franchise.id', id)
      .groupBy('season_franchise_iteration.league')

    return query
  },

  /**
   * Query for teams that meet criteria
   *
   * @param by Query filters
   * @param by.leagues Leagues to query
   * @param by.ids Team ids to query
   * @param by.active Query for only active teams
   * @returns All teams that meet criteria
   */
  query: async (by: { leagues?: string[]; ids?: number[]; active?: boolean }): Promise<Team[]> => {
    let query = db
      .select()
      .column('franchise.id')
      .column('franchise_iteration.represents')
      .column('franchise_iteration.name')
      .column('franchise.primary_league as primaryLeague')
      .from('franchise')
      .join('franchise_iteration', function() {
        this.on('franchise_iteration.franchise', 'franchise.id').andOn(
          'franchise_iteration.iteration',
          'franchise.iteration'
        )
      })

    if (by.leagues) {
      query = query.whereIn('franchise.primary_league', by.leagues)
    }

    if (by.ids) {
      query = query.whereIn('franchise.id', by.ids)
    }

    if (by.active != undefined) {
      query = query.where('franchise.active', by.active)
    }

    return query
  }
}

/**
 * Query that will return every game in the database,
 * in the format specified by the Game interface
 */
const baseGameQuery = db
  .column('game.id')
  .column('game.league')
  .column('game.season')
  .column('game.game_date as date')
  .column(
    db.raw(
      "jsonb_build_object( \
        'id', game.home_franchise, 'represents', fi_home.represents, \
        'name', fi_home.name, \
        'primaryLeague', f_home.primary_league \
       ) as home"
    )
  )
  .column('game.home_score')
  .column(
    db.raw(
      "jsonb_build_object( \
        'id', game.away_franchise, 'represents', fi_away.represents, \
        'name', fi_away.name, \
        'primaryLeague', f_away.primary_league \
       ) as away"
    )
  )
  .column('game.away_score')
  .column('game.title')
  .column('game.venue')
  .from('game')
  .join('season_franchise_iteration as sfi_home', function() {
    this.on('sfi_home.league', 'game.league')
      .andOn('sfi_home.season', 'game.season')
      .andOn('sfi_home.franchise', 'game.home_franchise')
  })
  .join('franchise_iteration as fi_home', function() {
    this.on('fi_home.franchise', 'sfi_home.franchise').andOn(
      'fi_home.iteration',
      'sfi_home.iteration'
    )
  })
  .join('franchise as f_home', 'f_home.id', 'game.home_franchise')
  .join('season_franchise_iteration as sfi_away', function() {
    this.on('sfi_away.league', 'game.league')
      .andOn('sfi_away.season', 'game.season')
      .andOn('sfi_away.franchise', 'game.away_franchise')
  })
  .join('franchise_iteration as fi_away', function() {
    this.on('fi_away.franchise', 'sfi_away.franchise').andOn(
      'fi_away.iteration',
      'sfi_away.iteration'
    )
  })
  .join('franchise as f_away', 'f_away.id', 'game.away_franchise')

export const GameService = {
  /**
   * Get a game by its id
   *
   * @param id The game's id
   * @returns The specified game, or undefined if no such game
   *          is found
   */
  get: async (id: number): Promise<Game> => {
    const query = baseGameQuery
      .clone()
      .where('game.id', id)
      .first()

    return query
  },

  /**
   * Return all games that meet the given criteria
   *
   * @param by Query filters
   * @param by.ids Game ids to query
   * @param by.leagues Leagues to query
   * @param by.seasons Seasons to query
   * @param by.teams Teams to query
   * @param by.date Dates to query
   * @param by.dateRange Range of dates to query by
   * @param by.dateRange.start Query for games after dateRange.start, inclusive
   * @param by.dateRange.end Query for games before dateRange.end, inclusive
   * @param paginate Parameters for paginating results
   * @param paginate.on Parameters that mark the end of a previous page
   * @param paginate.on.date The last game date returned by the previous query
   * @param paginate.on.id The last game id returned by the previous query
   * @param paginate.entries - The number of games returned by the query
   * @returns All games that meet the given criteria, subject to the
   *          query's pagination
   */
  query: async (
    by: {
      ids?: number[]
      leagues?: string[]
      seasons?: number[]
      teams?: number[]
      dates?: string[]
      dateRange?: { start?: string; end?: string }
      order?: SortOrder
    },
    paginate?: {
      on?: { date: string; id: number }
      entries: number
    }
  ): Promise<Game[]> => {
    let query = baseGameQuery.clone()

    /*
     * Order query primarily by game_date, then by ascending ids.
     * If date ordering is not specified, choose DESC by default
     */
    const dateOrdering: string = by.order ? by.order : SortOrder.DESC
    query = query.orderBy([
      { column: 'game.game_date', order: dateOrdering },
      { column: 'game.id', order: SortOrder.ASC }
    ])

    // Filter query by the criteria given
    if (by.ids) {
      query = query.whereIn('game.id', by.ids)
    }

    if (by.leagues) {
      query = query.whereIn('game.league', by.leagues)
    }

    if (by.seasons) {
      query = query.whereIn('game.season', by.seasons)
    }

    if (by.teams) {
      query = query.where(function() {
        this.whereIn('game.home_franchise', by.teams).orWhereIn('game.away_franchise', by.teams)
      })
    }

    if (by.dates) {
      query = query.whereIn('game.game_date', by.dates)
    }

    if (by.dateRange) {
      if (by.dateRange.start && by.dateRange.end) {
        query = query.whereBetween('game.game_date', [by.dateRange.start, by.dateRange.end])
      } else if (by.dateRange.start) {
        query = query.where('game.game_date', '>=', by.dateRange.start)
      } else if (by.dateRange.end) {
        query = query.where('game.game_date', '<=', by.dateRange.end)
      }
    }

    // Paginate the query
    if (paginate) {
      // Limit the query to the number of entries specified
      query = query.limit(paginate.entries)

      if (paginate.on) {
        /*
         * Query for games that have:
         *
         * The same game_date as paginate.on.date and an id
         * greater than paginate.on.id
         *
         * OR
         *
         * A game_date after paginate.on.date, where the
         * game_date ordering is determined by dateOrdering
         */
        const dateOrderingOperator: string = dateOrdering == SortOrder.ASC ? '>' : '<'

        query = query.where(function() {
          this.where(function() {
            this.where('game.game_date', paginate.on.date).andWhere('game.id', '>', paginate.on.id)
          }).orWhere('game.game_date', dateOrderingOperator, paginate.on.date)
        })
      }
    }

    return query
  },

  /**
   * Create a game
   *
   * @param attributes Game data to be stored
   * @param attributes.date The game's date
   * @param attributes.league The league in which the game was played
   * @param attributes.homeId The id of the home team
   * @param attributes.awayId The id of the away team
   * @param attributes.home_score The number of points scored by the home team
   * @param attributes.away_score The number of points scored by the away team
   * @param attributes.title A title for the game
   * @param attributes.venue The venue where the game took place
   * @returns The game's id
   */
  create: async (attributes: {
    date: string
    league: string
    homeId: number
    awayId: number
    home_score: number
    away_score: number
    title?: string
    venue?: string
  }): Promise<number> => {
    const transaction = db.transaction(async trx => {
      const [season]: number[] = await trx('season')
        .pluck('start_year')
        .where('league', attributes.league)
        .andWhereRaw('?::date <@ span', [attributes.date])
        .limit(1)

      if (!season) {
        throw new InvalidDataError('Date out of season OR no such league', ['date', 'league'])
      }

      const [id]: number[] = await trx('game').insert(
        {
          season: season,
          game_date: attributes.date,
          league: attributes.league,
          home_franchise: attributes.homeId,
          home_score: attributes.home_score,
          away_franchise: attributes.awayId,
          away_score: attributes.away_score,
          title: attributes.title || null,
          venue: attributes.venue || null
        },
        'game.id'
      )

      return id
    })

    try {
      return await transaction
    } catch (err) {
      if (err.name == 'PostgresError') {
        switch (err.errorClass) {
          case PgErrorClass.INTEGRITY_CONSTRAINT_VIOLATION:
          case PgErrorClass.DATA_EXCEPTION:
            throw new InvalidDataError('Invalid data')
          default:
            throw err
        }
      } else {
        throw err
      }
    }
  },

  /**
   * Edit a game
   *
   * @param id The game to edit
   * @param attributes Attributes to be changed
   * @param attributes.league The league in which the game was played
   * @param attributes.homeId The id of the home team
   * @param attributes.awayId The id of the away team
   * @param attributes.home_score The number of points scored by the home team
   * @param attributes.away_score The number of points scored by the away team
   * @param attributes.title A title for the game
   * @param attributes.venue The venue where the game took place
   */
  edit: async (
    id: number,
    attributes: {
      date?: string
      league?: string
      homeId?: number
      awayId?: number
      home_score?: number
      away_score?: number
      title?: string
      venue?: string
    }
  ): Promise<void> => {
    const transaction = db.transaction(async trx => {
      let season = undefined

      if (attributes.date || attributes.league) {
        if (attributes.date && attributes.league) {
          const resultArray: number[] = await trx('season')
            .pluck('start_year')
            .where('league', attributes.league)
            .whereRaw('?::date <@ span', [attributes.date])

          season = resultArray[0]
        } else if (attributes.date) {
          const resultArray: number[] = await trx('season')
            .pluck('start_year')
            .join('game', 'game.league', 'season.league')
            .where('game.id', id)
            .whereRaw('?::date <@ span', [attributes.date])

          season = resultArray[0]
        } else if (attributes.league) {
          const resultArray: number[] = await trx('season')
            .pluck('start_year')
            .join('game', 'game.game_date', '<@', 'season.span')
            .where('game.id', id)
            .where('season.league', attributes.league)

          season = resultArray[0]
        }

        if (!season) {
          throw new InvalidDataError('Date out of season OR no such league')
        }
      }

      return trx('game')
        .where({ id })
        .update({
          season: season,
          game_date: attributes.date,
          league: attributes.league,
          home_franchise: attributes.homeId,
          home_score: attributes.home_score,
          away_franchise: attributes.awayId,
          away_score: attributes.away_score,
          title: attributes.title,
          venue: attributes.venue
        })
    })

    try {
      await transaction
    } catch (err) {
      if (err.name == 'PostgresError') {
        switch (err.errorClass) {
          case PgErrorClass.INTEGRITY_CONSTRAINT_VIOLATION:
          case PgErrorClass.DATA_EXCEPTION:
            throw new InvalidDataError('Invalid data')
          default:
            throw err
        }
      } else {
        throw err
      }
    }
  },

  /**
   * Delete a game
   *
   * @param id The id of the game to be deleted
   * @returns The number of rows deleted
   */
  delete: async (id: number): Promise<number> => {
    return db('game')
      .del()
      .where({ id })
  }
}

export const ResourceService = {
  /**
   * Get a resource by id
   *
   * @param id The resource's id
   * @returns The resource, or undefined if no such resource is found
   */
  get: async (id: number): Promise<Resource> => {
    return db('resource')
      .where({ id })
      .first()
  },

  /**
   * Query for resources that meet criteria
   *
   * @param by Query filters
   * @param by.games Query resources for specified games
   * @param by.id Query for resources by their id
   * @returns All resources that meet criteria
   */
  query: async (by: { games?: number[]; ids?: number[] }): Promise<Resource[]> => {
    let query = db('resource')

    if (by.games) {
      query = query.whereIn('game', by.games)
    }

    if (by.ids) {
      query = query.whereIn('id', by.ids)
    }

    return query
  },

  /**
   * Create a resource for a game
   *
   * @param attributes Resource data to be stored
   * @param attributes.game The game's id
   * @param attributes.gameSegment meta, pre-game, in-game, or post-game
   * @param attributes.type link
   * @param attributes.url The url of a link resource
   * @returns The id of the new resource
   */
  create: async (attributes: {
    game: number
    gameSegment: string
    type: string
    title?: string
    url?: string
  }): Promise<number> => {
    const transaction = db.transaction(async trx => {
      switch (attributes.type) {
        case 'link':
          let { domain, tld } = parseDomain(attributes.url) || { domain: null, tld: null }
          if (!domain) {
            throw new InvalidDataError('Invalid URL', 'url')
          }
          domain = `${domain}.${tld}`
          const whiteListQuery = await trx('link_resource_whitelist').where({ domain })
          const whiteListed = whiteListQuery.length > 0
          if (!whiteListed) {
            throw new InvalidDataError('Domain not allowed', 'url')
          }
          break
      }

      let args = { ...attributes } as any
      args.game_segment = args.gameSegment
      delete args.gameSegment

      const [id]: number[] = await trx('resource').insert(args, 'resource.id')

      return id
    })

    try {
      return await transaction
    } catch (err) {
      if (err.name == 'PostgresError') {
        switch (err.errorClass) {
          case PgErrorClass.INTEGRITY_CONSTRAINT_VIOLATION:
          case PgErrorClass.DATA_EXCEPTION:
            throw new InvalidDataError('Invalid data')
          default:
            throw err
        }
      } else {
        throw err
      }
    }
  },

  /**
   * Edit a resource
   *
   * @param id The resource's id
   * @param attributes Attributes to be changed
   * @param attributes.gameSegment meta, pre-game, in-game, or post-game
   * @param attributes.title The resource's title
   */
  edit: async (
    id: number,
    attributes: {
      gameSegment?: string
      title?: string
    }
  ): Promise<void> => {
    let args = { ...attributes } as any
    args.game_segment = args.gameSegment
    delete args.gameSegment

    try {
      await db('resource')
        .update(args)
        .where({ id })
    } catch (err) {
      if (err.name == 'PostgresError') {
        switch (err.errorClass) {
          case PgErrorClass.INTEGRITY_CONSTRAINT_VIOLATION:
          case PgErrorClass.DATA_EXCEPTION:
            throw new InvalidDataError('Invalid data')
          default:
            throw err
        }
      } else {
        throw err
      }
    }
  },

  /**
   * Delete a resource
   *
   * @param id The id of the resource to be deleted
   * @returns The number of resources deleted
   */
  delete: async (id: number): Promise<number> => {
    return db('resource')
      .del()
      .where({ id })
  }
}

export const AccountService = {
  /**
   * Creates a new account, optionally with the given role
   *
   * @param username The account's username
   * @param password The account's password
   */
  create: async (username: string, password: string): Promise<void> => {
    const query = db('account').insert({ username, password })

    try {
      await query
    } catch (err) {
      if (err.name == 'PostgresError') {
        if (err.errorCondition == PgError.UNIQUE_VIOLATION) {
          throw new ConflictError(err.message)
        } else {
          throw err
        }
      } else {
        throw err
      }
    }
  },

  /**
   * Edits an existing account's password
   * or role
   *
   * @param username The username of the account being altered
   * @param attributes The attributes being changed
   * @param attributes.password The account's password
   * @param attributes.role The account's role
   */
  edit: async (
    username: string,
    attributes: { password: string; role: AccountRole }
  ): Promise<number> => {
    const query = db('account')
      .where({ username })
      .update(attributes)

    return query
  },

  /**
   * Authenticates a username and password against the database
   *
   * @param username The account's username
   * @param password The account's password
   * @returns The corresponding Account object, or null if the given
   *          account could not be authenticated
   */
  authenticate: async (username: string, password: string): Promise<Account> => {
    const query = db('account')
      .column('username')
      .column('role')
      .column(db.raw('(password = crypt(?, password)) as authenticated', [password]))
      .where({ username })

    let results = await query

    if (!results.length || !results[0].authenticated) {
      return null
    } else {
      return { username: results[0].username, role: results[0].role }
    }
  },

  /**
   * Creates a login session for the given username
   *
   * @param username Username associated with account that is logged in
   * @returns The session's id and signature
   */
  createSession: async (username: string): Promise<{ id: string; signed: string }> => {
    const query = db('session')
      .insert({ username })
      .returning(['id', 'signed'])

    return (await query)[0]
  },

  /**
   * Get a session, validating that the given
   * signature matches the signature in the database
   *
   * @param id The session's id
   * @param signed The signature being challenged
   * @returns The session, or undefined if no such session is found
   */
  getSession: async (id: string, signed: string): Promise<Session> => {
    const query = db('session')
      .join('account', 'account.username', 'session.username')
      .column('session.id')
      .column(
        db.raw(
          `
          jsonb_build_object(
            'username', session.username,
            'role', account.role
          ) as account
          `
        )
      )
      .column(db.raw('(current_timestamp > expires) as expired'))
      .column(db.raw('(? = signed) as signed', signed))
      .where({ id })
      .first()

    return query
  },

  /**
   * Deletes a session from the database
   *
   * @param id The session's id
   * @returns The number of records deleted
   */
  destroySession: async (id: string): Promise<number> => {
    return db('session')
      .del()
      .where({ id })
  }
}
