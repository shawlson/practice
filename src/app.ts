import fastify = require('fastify')
import fastifyCookie = require('fastify-cookie')
import ajv = require('ajv')

import { session } from './middleware'
import { InvalidDataError } from './errors'
import { LeagueRoutes, TeamRoutes, GameRoutes, ResourceRoutes, AccountRoutes } from './routes'

const ajvInstance = new ajv({
  removeAdditional: 'all',
  useDefaults: true,
  coerceTypes: 'array',
  allErrors: true
})

const server = fastify()
server.setSchemaCompiler(schema => ajvInstance.compile(schema))
server.register(fastifyCookie)
server.register(session)
server.register(LeagueRoutes)
server.register(TeamRoutes)
server.register(GameRoutes)
server.register(ResourceRoutes)
server.register(AccountRoutes)

server.setNotFoundHandler(async (request, reply) => {
  reply.code(404)
  return { message: 'Not found' }
})

server.setErrorHandler(async (error, request, reply) => {
  if (error instanceof InvalidDataError) {
    reply.code(400)
    return { message: error.message, field: error.fields || undefined }
  } else if (error.validation) {
    reply.code(400)
    return { message: 'Bad request' }
  } else {
    console.error(error)
    reply.code(500)
    return { message: 'Internal error' }
  }
})

const start = async () => {
  try {
    await server.listen(process.env.API_PORT)
  } catch (err) {
    server.log.error(err)
    process.exit(1)
  }
}

start()
