const id = {
  type: 'integer'
}

const date = {
  type: 'string',
  format: 'date'
}

const leagueName = {
  type: 'string',
  maxLength: 8
}

const leagueDescription = {
  type: 'string',
  maxLength: 32
}

const league = {
  type: 'object',
  properties: {
    name: leagueName,
    description: leagueDescription
  }
}

const team = {
  type: 'object',
  properties: {
    id: id,
    represents: { type: 'string', maxLength: 64 },
    name: { type: 'string', maxLength: 64 },
    primaryLeague: leagueName
  }
}

const season = {
  type: 'integer'
}

const score = {
  type: 'integer'
}

const title = { type: ['string', 'null'], maxLength: 64 }

const venue = { type: ['string', 'null'], maxLength: 32 }

const game = {
  type: 'object',
  properties: {
    id: id,
    league: leagueName,
    season: season,
    date: date,
    home: team,
    home_score: score,
    away: team,
    away_score: score,
    title: title,
    venue: venue
  }
}

const gameSegment = {
  type: 'string',
  enum: ['meta', 'pre-game', 'in-game', 'post-game']
}

const baseResource = {
  type: 'object',
  properties: {
    id: id,
    game: id,
    title: title,
    gameSegment: gameSegment,
    type: { type: 'string', enum: ['link'] }
  }
}

const linkResourceUrl = { type: 'string', maxLength: 128 }

const linkResource = {
  type: 'object',
  properties: {
    ...baseResource.properties,
    type: { type: 'string', enum: ['link'] },
    url: linkResourceUrl
  }
}

const username = { type: 'string', minLength: 1, maxLength: 16 }
const password = { type: 'string', minLength: 1, maxLength: 72 }
const role = { type: 'string', enum: ['admin', 'contributor', 'friend'] }

export const LeagueSchemas = {
  get: {
    params: { name: leagueName },
    response: {
      200: {
        type: 'object',
        properties: {
          name: leagueName,
          description: leagueDescription,
          seasons: {
            type: 'array',
            items: season
          }
        }
      }
    }
  },

  query: {
    querystring: {
      name: { type: 'array', items: leagueName }
    },
    response: {
      200: {
        type: 'array',
        items: league
      }
    }
  }
}

export const TeamSchemas = {
  get: {
    params: { id: id },
    response: {
      200: {
        type: 'object',
        properties: {
          team: team,
          history: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                league: leagueName,
                seasons: { type: 'array', items: season }
              }
            }
          }
        }
      }
    }
  },

  query: {
    querystring: {
      id: { type: 'array', items: id },
      active: { type: 'boolean', default: true },
      league: { type: 'array', items: leagueName }
    },
    response: {
      200: {
        type: 'array',
        items: team
      }
    }
  }
}

export const GameSchemas = {
  get: {
    params: { id: id },
    response: {
      200: game
    }
  },

  query: {
    querystring: {
      id: { type: 'array', items: id },
      league: { type: 'array', items: leagueName },
      season: { type: 'array', items: season },
      team: { type: 'array', items: id },
      date: { type: 'array', items: date },
      startDate: date,
      endDate: date,
      order: { type: 'string', enum: ['asc', 'desc'], default: 'desc' },
      limit: { type: 'integer', default: 10 },
      pageDate: date,
      pageId: id
    },
    response: {
      200: {
        type: 'array',
        items: game
      }
    }
  },

  post: {
    body: {
      type: 'object',
      required: ['league', 'date', 'homeId', 'awayId', 'home_score', 'away_score'],
      properties: {
        league: leagueName,
        date: date,
        homeId: id,
        awayId: id,
        home_score: score,
        away_score: score,
        title: title,
        venue: venue
      }
    }
  },

  patch: {
    params: { id: id },
    body: {
      type: 'object',
      properties: {
        league: leagueName,
        date: date,
        homeId: id,
        awayId: id,
        home_score: score,
        away_score: score,
        title: title,
        venue: venue
      }
    }
  },

  delete: {
    params: { id: id }
  }
}

export const ResourceSchemas = {
  get: {
    params: { id: id },
    response: {
      200: {
        type: 'object',
        properties: {
          ...baseResource.properties,
          url: { type: ['string', 'null'], maxLength: 128 }
        }
      }
    }
  },

  query: {
    querystring: {
      type: 'object',
      anyOf: [{ required: ['id'] }, { required: ['game'] }],
      properties: {
        id: { type: 'array', items: id },
        game: { type: 'array', items: id }
      }
    },
    response: {
      200: {
        type: 'array',
        items: { anyOf: [linkResource] }
      }
    }
  },

  post: {
    body: {
      type: 'object',
      required: ['game', 'gameSegment', 'type'],
      anyOf: [{ required: ['url'] }],
      properties: {
        game: id,
        gameSegment: gameSegment,
        type: { type: 'string', enum: ['link'] },
        title: title,
        url: linkResourceUrl
      }
    }
  },

  patch: {
    params: { id: id },
    body: {
      type: 'object',
      properties: {
        gameSegment: gameSegment,
        title: title
      }
    }
  },

  delete: {
    params: { id: id }
  }
}

export const AccountSchemas = {
  post: {
    body: {
      type: 'object',
      required: ['username', 'password'],
      properties: {
        username: username,
        password: password
      }
    }
  },

  login: {
    body: {
      type: 'object',
      required: ['username', 'password'],
      properties: {
        username: username,
        password: password
      }
    }
  },

  patch: {
    params: { username: username },
    body: {
      type: 'object',
      properties: {
        password: password,
        role: role
      }
    }
  }
}
