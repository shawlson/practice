import fp = require('fastify-plugin')

import { AccountService } from './services'
import { Session, AccountRole } from './types'

export const session = fp(sessionPlugin)

function sessionPlugin(fastify, options, next) {
  fastify.decorateRequest('session', null)

  fastify.addHook('onRequest', async (request, reply) => {
    if (request.cookies.session && request.cookies.signed) {
      const session: Session = await AccountService.getSession(
        request.cookies.session,
        request.cookies.signed
      )

      if (session && session.signed && !session.expired) {
        request.session = session
      }
    }
  })

  fastify.addHook('onSend', async (request, reply, payload) => {
    if (request.session && !request.session.expired) {
      reply.header('account', request.session.account.username)
      reply.header('role', request.session.account.role)
    }
  })

  next()
}

export function authorizeTo(roles: AccountRole[]) {
  return async function(request, reply) {
    if (!request.session || !roles.includes(request.session.account.role)) {
      reply.code(403)
      reply.send({ message: 'Forbidden' })
    }
  }
}
