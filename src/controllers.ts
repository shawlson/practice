import fastify = require('fastify')
import { IncomingMessage, ServerResponse } from 'http'

import { InvalidDataError, ConflictError } from './errors'
import { League, Team, TeamLeagueHistory, Game, Resource, Account } from './types'
import {
  LeagueService,
  TeamService,
  GameService,
  ResourceService,
  AccountService
} from './services'

export const LeagueController = {
  get: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    const league = await LeagueService.get(req.params.name)
    if (league) {
      reply.code(200)
      return league
    } else {
      reply.code(404)
      return { message: 'League not found' }
    }
  },

  query: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    const leagues: League[] = await LeagueService.query(req.query.name)
    if (leagues.length) {
      reply.code(200)
      return leagues
    } else {
      reply.code(404)
      return { message: 'No leagues found' }
    }
  }
}

export const TeamController = {
  get: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    const team: Team = await TeamService.get(req.params.id)
    if (team) {
      const history: TeamLeagueHistory[] = await TeamService.getLeagueHistory(req.params.id)
      reply.code(200)
      return { team, history }
    } else {
      reply.code(404)
      return { message: 'Team not found' }
    }
  },

  query: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    const teams: Team[] = await TeamService.query({
      leagues: req.query.league,
      ids: req.query.id,
      active: req.query.active
    })

    if (teams.length) {
      reply.code(200)
      return teams
    } else {
      reply.code(404)
      return { message: 'No teams found' }
    }
  }
}

export const GameController = {
  get: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    const game: Game = await GameService.get(req.params.id)
    if (game) {
      reply.code(200)
      return game
    } else {
      reply.code(404)
      return { message: 'Game not found' }
    }
  },

  query: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    /**
     * 🤢🤮
     * No req.query.limit? No pagination.
     * No (req.query.pageId && req.query.pageDate)? No "pivot" point.
     */
    const paginate = req.query.limit
      ? {
          entries: req.query.limit,
          on:
            req.query.pageId && req.query.pageDate
              ? { id: req.query.pageId, date: req.query.pageDate }
              : null
        }
      : null

    const games: Game[] = await GameService.query(
      {
        ids: req.query.id,
        leagues: req.query.league,
        seasons: req.query.season,
        teams: req.query.team,
        dates: req.query.date,
        dateRange: {
          start: req.query.startDate,
          end: req.query.endDate
        },
        order: req.query.order
      },
      paginate
    )
    if (games.length) {
      reply.code(200)
      return games
    } else {
      reply.code(404)
      return { message: 'No games found' }
    }
  },

  post: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    const id: number = await GameService.create(req.body)

    reply.code(201)
    reply.header('Location', `/games/${id}`)
    return { message: 'Game created' }
  },

  patch: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    await GameService.edit(req.params.id, req.body)

    reply.code(200)
    return { message: 'Game updated' }
  },

  delete: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    await GameService.delete(req.params.id)

    reply.code(200)
    return { message: 'Game deleted' }
  }
}

export const ResourceController = {
  get: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    const resource: Resource = await ResourceService.get(req.params.id)
    if (resource) {
      reply.code(200)
      return resource
    } else {
      reply.code(404)
      return { message: 'Resource not found' }
    }
  },

  query: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    const resources: Resource[] = await ResourceService.query({
      games: req.query.game,
      ids: req.query.id
    })

    if (resources.length) {
      reply.code(200)
      return resources
    } else {
      reply.code(404)
      return { message: 'No resources found' }
    }
  },

  post: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    switch (req.body.type) {
      case 'link':
        if (!req.body.url) {
          throw new InvalidDataError('url needed for link', 'url')
        }
        break
    }

    const id: number = await ResourceService.create(req.body)

    reply.code(201)
    reply.header('Location', `/resources/${id}`)
    return { message: 'Resource created' }
  },

  patch: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    await ResourceService.edit(req.params.id, req.body)

    reply.code(200)
    return { message: 'Resource updated' }
  },

  delete: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    await ResourceService.delete(req.params.id)

    reply.code(200)
    return { message: 'Resource deleted' }
  }
}

const enum AccountRole {
  ADMIN = 'admin',
  CONTRIBUTOR = 'contributor',
  FRIEND = 'friend'
}

export const AccountController = {
  post: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    try {
      await AccountService.create(req.body.username, req.body.password)
    } catch (err) {
      if (err instanceof ConflictError) {
        reply.code(409)
        return { message: 'Username already exists' }
      } else {
        throw err
      }
    }

    const sessionDetails = await AccountService.createSession(req.body.username)
    req.session = {
      id: sessionDetails.id,
      account: {
        username: req.body.username,
        role: AccountRole.FRIEND
      },
      expired: false,
      signed: true
    }

    reply.code(200)
    reply.setCookie('session', sessionDetails.id, {
      httpOnly: true,
      maxAge: parseInt(process.env.SESSION_LENGTH)
    })
    reply.setCookie('signed', sessionDetails.signed, {
      httpOnly: true,
      maxAge: parseInt(process.env.SESSION_LENGTH)
    })

    return { message: 'User created' }
  },

  patch: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    // Only an admin can edit someone else's account or change an account's
    // role
    if (
      req.session.account.role != AccountRole.ADMIN &&
      (req.body.role || req.params.username != req.session.account.username)
    ) {
      reply.code(403)
      return { message: 'Forbidden' }
    }

    let usersEdited: number = await AccountService.edit(req.params.username, req.body)
    if (usersEdited == 1) {
      reply.code(200)
      return { message: 'Account updated' }
    } else {
      reply.code(404)
      return { message: 'No such account' }
    }
  },

  login: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    const account: Account = await AccountService.authenticate(req.body.username, req.body.password)
    if (!account) {
      reply.code(401)
      return { message: 'Unauthorized' }
    }

    const sessionDetails = await AccountService.createSession(account.username)
    req.session = {
      id: sessionDetails.id,
      account: account,
      expired: false,
      signed: true
    }

    reply.code(200)
    reply.setCookie('session', sessionDetails.id, {
      httpOnly: true,
      maxAge: parseInt(process.env.SESSION_LENGTH)
    })
    reply.setCookie('signed', sessionDetails.signed, {
      httpOnly: true,
      maxAge: parseInt(process.env.SESSION_LENGTH)
    })

    return { message: 'Login successful' }
  },

  logout: async (
    req: fastify.FastifyRequest<IncomingMessage>,
    reply: fastify.FastifyReply<ServerResponse>
  ) => {
    if (req.session) {
      await AccountService.destroySession(req.session.id)
      req.session.expired = true
    }

    return { message: 'Logout successful' }
  }
}
